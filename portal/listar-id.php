<?php 
session_start(); //iniciar sessão

include("functions/function-lista.php");
include("functions/function.php");
include("../functions/conexao.php");

unset($_SESSION['novo']); // limpa o parametro da sessão 'novo'

//verifica se há sessão de login
//se não houver redireciona para a página de login
if(isset($_SESSION['autenticado'])){
	$id = pegarID();
}else {
	header("location: ../login.php");
	die();
}
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Woman Care - Painel Administrativo</title>
	<link href="css/main.css" type="text/css" rel="stylesheet" />

	<!-- Fonte Awesome Bootstrap -->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

	<!-- jQuery Google -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Validação do questionario -->
	<script type="text/javascript" src="js/validar_quest.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

</head>

<body>
	<!-- Menu da página -->
	<?php include "menu_adm.php" ?>

	<!-- Inicio de Conteúdo -->
	<div class="main">
		<div class="in-main" id="conteudo">

			<div class="titulo-table">
				<span>Questionário</span>
			</div>
			<div class="questionario">
				<?php 
				//se o parámetro do ID do questionário for passado pelo GET ele vai imprimir o questionario escolhido
				//se não houver GET ele vai voltar para a lista
				if(isset($_GET['idQ'])){
					imprimirQuestionarioID($_GET['idQ']);
				} else {
					header("location: lista.php");
					die();
				}				
				?>
				<a href="lista.php" title="Voltar para a lista">Voltar</a>
			</div>

		</div>
	</div>

</body>

</html>