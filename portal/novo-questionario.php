<?php 
session_start(); // iniciar sessão

include("functions/function.php");
include("functions/function-criar.php");
include("functions/function-cadastrar.php");
include("../functions/conexao.php");

$confirmar = false;

//verifica se há sessão de login
//se não houver redireciona para a página de login
if(isset($_SESSION['autenticado'])){
	$id = $_SESSION['id'] = pegarID();
}else {
	header("location: ../login.php");
	die();
}


?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Woman Care - Painel Administrativo</title>
	<link href="css/main.css" type="text/css" rel="stylesheet" />

	<!-- Fonte Awesome Bootstrap -->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

	<!-- jQuery Google -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Validação do questionario -->
	<script type="text/javascript" src="js/validar_quest.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

</head>

<body>
	<!-- Menu da página -->
	<?php include "menu_adm.php" ?>

	<!-- Inicio de Conteúdo -->
	<div class="main">
		<div class="in-main" id="conteudo">

			<div class="titulo-table">
				<span>Novo Questionário</span>
			</div>
			<div class="questionario">
				<form action="" id="quest" name="quest" method="POST">
					<?php

					//verifica se o usuário abriu uma nova sessão de questionário 
					//se não redireciona para a página de lista de questionário
					if(isset($_SESSION['novo'])){
						$sequecia = criarQuestionario(); // Cria um novo questinário e salva o array retornado pela função em uma variável

						if(isset($_POST["Done"])){
							$cadastro = validarQuestionario($sequecia); // Quando o usuário finaliza o questionário chama a função de validação para cadastro
							
							if($cadastro){ // se o cadastro der true, apresenta a mensagem de cadastrado
								echo "<script>alert('Questionário Cadastrado!'); window.location.href='lista.php';</script>";
							}else { // senão erro de cadastro
								echo "<script>alert('Erro ao cadastrar Questionário! Tente novamente!'); window.location.href = 'lista.php';</script>";
							}
						}
					}else{
						header("location: lista.php");
					}

					?>

					<button id="btnSubmit" name='Done' value="Login">Finalizar Questionario</button>
				</form>
			</div>

		</div>
	</div>

</body>

</html>