<div class="header">
	<ul>
		<li class="logout">
			<a href="?sair" title="Sair">
				<span>
					<i class="fa fa-power-off"></i>
				</span>
				<span>Sair</span>
			</a>
		</li>
	</ul>
</div>

<div class="nav-main">
	<nav>
		<ul>
			<li>
				<a href="index.php" id="usuario" title="Menú de usuário">
					<span>
						<i class="fa fa-user"></i>
					</span>
					<span>Usuário</span>
				</a>
			</li>
			<li>
				<a href="lista.php" id="lista" title="Lista de Questionários Realizados">
					<span>
						<i class="fa fa-file-text-o"></i>
					</span>
					<span>Lista de Questionários</span>
				</a>
			</li>

		</ul>
	</nav>
</div>