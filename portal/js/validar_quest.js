$(document).ready(function () {

	$('#quest').validate({
		rules: {
			1: {
				required: true
			},
			2: {
				required: true
			},
			3: {
				required: true
			},
			4: {
				required: true
			},
			5: {
				required: true
			},
			6: {
				required: true
			},
			7: {
				required: true
			},
			8: {
				required: true
			},
			9: {
				required: true
			},
			10: {
				required: true
			},
			11: {
				required: true
			},
			12: {
				required: true
			},
			13: {
				required: true
			},
			14: {
				required: true
			},
			15: {
				required: true
			},
			16: {
				required: true
			},
			17: {
				required: true
			},
			18: {
				required: true
			},
			19: {
				required: true
			},
			20: {
				required: true
			}
		},
		messages: {
			1: {
				required: "Preenchimento Obrigatório!</br>"
			},
			2: {
				required: "Preenchimento Obrigatório!</br>"
			},
			3: {
				required: "Preenchimento Obrigatório!</br>"
			},
			4: {
				required: "Preenchimento Obrigatório!</br>"
			},
			5: {
				required: "Preenchimento Obrigatório!</br>"
			},
			6: {
				required: "Preenchimento Obrigatório!</br>"
			},
			7: {
				required: "Preenchimento Obrigatório!</br>"
			},
			8: {
				required: "Preenchimento Obrigatório!</br>"
			},
			9: {
				required: "Preenchimento Obrigatório!</br>"
			},
			10: {
				required: "Preenchimento Obrigatório!</br>"
			},
			11: {
				required: "Preenchimento Obrigatório!</br>"
			},
			12: {
				required: "Preenchimento Obrigatório!</br>"
			},
			13: {
				required: "Preenchimento Obrigatório!</br>"
			},
			14: {
				required: "Preenchimento Obrigatório!</br>"
			},
			15: {
				required: "Preenchimento Obrigatório!</br>"
			},
			16: {
				required: "Preenchimento Obrigatório!</br>"
			},
			17: {
				required: "Preenchimento Obrigatório!</br>"
			},
			18: {
				required: "Preenchimento Obrigatório!</br>"
			},
			19: {
				required: "Preenchimento Obrigatório!</br>"
			},
			20: {
				required: "Preenchimento Obrigatório!</br>"
			}
		},
		errorPlacement: function (label, element) {
			label.insertBefore(element);
		}

	});

});