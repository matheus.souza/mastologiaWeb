$(document).ready(function () {
	var totalLinhas = $('#tblData').find('tbody tr:has(td)').length;
	var limitePorPagina = 6;
	var todasPag = Math.ceil(totalLinhas / limitePorPagina);
	var $pag = $('<br/><br/><div id="pag"></div>');
	for (i = 0; i < todasPag; i++) {
		$('<span class="pageNumber">&nbsp;' + (i + 1) + '</span>').appendTo($pag);
	}
	$pag.appendTo('#tblData');

	$('.pageNumber').hover(
		function () {
			$(this).addClass('focus');
		},
		function () {
			$(this).removeClass('focus');
		}
	);

	$('table').find('tbody tr:has(td)').hide();
	var tr = $('table tbody tr:has(td)');
	for (var i = 0; i <= limitePorPagina - 1; i++) {
		$(tr[i]).show();
	}
	$('span').click(function (event) {
		$('#tblData').find('tbody tr:has(td)').hide();
		var nInic = ($(this).text() - 1) * limitePorPagina;
		var nFinal = $(this).text() * limitePorPagina - 1;
		for (var i = nInic; i <= nFinal; i++) {
			$(tr[i]).show();
		}
	});

	$("#novoquestionario").click(function () {
		$("#conteudo").load('pag/usuario.php');
	});
});