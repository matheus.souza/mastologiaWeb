<?php 
session_start(); //iniciar sessão

include("functions/function.php");
include("functions/function-lista.php");
include("../functions/conexao.php");

unset($_SESSION['novo']); // limpa o parametro da sessão 'novo'

//verifica se há sessão de login
//se não houver redireciona para a página de login
if(isset($_SESSION['autenticado'])){
	$id = $_SESSION['id'] = pegarID();
}else {
	header("location: ../login.php");
	die();
}

//verifica se houve algum post quando clica em novo questionario
//inicia o parametro de novoQuestionario para cria-lo
//redireciona para a página novo-questionario
if(isset($_POST["novoQuestionario"])){
	$_SESSION['novo'] = true;
	header("location: novo-questionario.php");
}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Woman Care - Painel Administrativo</title>
	<link href="css/main.css" type="text/css" rel="stylesheet" />

	<!-- Fonte Awesome Bootstrap -->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

	<!-- jQuery Google -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- efeitos de includ da main -->
	<script type="text/javascript" src="js/tabela.js"></script>

	<script type="text/javascript" src="js/jquery.tablesorter.pager.js"></script>
</head>

<body>
	<!-- Menu da página -->
	<?php include "menu_adm.php" ?>

	<!-- Inicio de Conteúdo -->
	<div class="main">
		<div class="in-main" id="conteudo">

			<div class="titulo-table">
				<span>Lista de Questionários</span>
			</div>

			<script src="_js/tabela.js" type="text/javascript"></script>

			<div class="tabela">
				<center>
					<table id="tblData">
						<thead>
							<tr>
								<th width="5%">Número</th>
								<th>Dia</th>
								<th>Quantidade de Acertos</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php 
								//função lista os questionarios do usuário cadastrados no banco
								listarQuestionarios($id);					
							?>
						</tbody>
					</table>
				</center>
				<form action="" id="novo" name="novo" method="POST">
					<button id="btnSubmit" name='novoQuestionario' value="Novo">Novo Questionario</button>
				</form>

			</div>
		</div>
	</div>

</body>

</html>