<?php 
// inicio da sessão
session_start();
include("functions/function.php");

unset($_SESSION['novo']); // limpando a variável da sessão novo

//verifica se há uma sessão de login, se sim ele pega os dados passados pelo login
// se não tiver sessão ele rentorna para a tela de login
if(isset($_SESSION["autenticado"])){
	$nome = pegarNome();
}else{
	header("Location: ../login.php");
	die();
}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Woman Care - Portal</title>
	<link href="css/main.css" type="text/css" rel="stylesheet" />

	<!-- Fonte Awesome Bootstrap -->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

	<!-- jQuery Google -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- efeitos de includ da main -->
	<script type="text/javascript" src="js/efeitopag.js"></script>

	<script type="text/javascript" src="js/jquery.tablesorter.pager.js"></script>
</head>

<body>
	<!-- Menu da página -->
	<?php include "menu_adm.php" ?>

	<!-- Inicio de Conteúdo -->
	<div class="main">
		<div class="in-main" id="conteudo">
			<div>
				<center class="shadow">
					<i class="fa fa-user"></i>

					<?php 
					echo "<h1>Bem vindo $nome!</h1>";//mostra o nome do usuário logado
					?>
				</center>
				<br/><br/>

				<h2>Lista de Materiais:</h2>
				<ul class="pdf">
					<li><a href="arquivos/Artigo1_Bioinformatica.pdf" target="_blank">A era da Bioinformática: seu Potencial e suas Implicações para as Ciências da Saúde</a></li>
					<li><a href="arquivos/Artigo2_Mastologia_B.pdf" target="_blank">Banco de Imagens Mamográficas para Treinamento na Interpretação de Imagens Digitais</a></li>
					<li><a href="arquivos/Artigo3_Mastologia_A.PDF" target="_blank"><em>Mammographic Assessment of a Geographically Defined Population at a Mastology Referral Hospital in São Paulo Brazil</em></a></li>
				</ul>
			</div>
		</div>
	</div>

</body>

</html>