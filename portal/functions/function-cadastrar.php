<?php
//função que verificar se alguma pergunta não foi respondida
function validarQuestionario($sequencia){
    $erro = false;
    $ok = false; 
    
    for($i=0; $i<sizeof($sequencia); $i++){
        $aux = $sequencia[$i]['pergunta'];

        if(isset($_POST["$aux"]) && ($_POST["$aux"]) != null){
            $sequencia[$i]['resposta'] = $_POST["$aux"];
           
        } else {
            $erro = true;
        }
    }

    if(!$erro){//se não tiver erro ele vai chamar a função de correção

        $ok = corrigirQuestionario($sequencia);
        unset($_SESSION['novo']);
    }
    return $ok;
}

// puxa o gabarito da pergunta no banco de dados e compara com a alternativa do usuário
function corrigirQuestionario($sequencia){
    $pdo = conn(); //abre conexão
    $acertos = 0;
    $ok = false;

    for($i=0; $i<sizeof($sequencia); $i++){

        if($pdo != null){

            $query = "SELECT id_perguntas, resposta FROM perguntas WHERE id_perguntas=:id";
            $result = execSQL($pdo, $query);
            $result -> bindValue(":id", $sequencia[$i]['pergunta'], PDO::PARAM_STR);
            $gabarito = $result -> execute();
            $row = $result->fetch(PDO::FETCH_ASSOC);

            if($row['resposta'] == $sequencia[$i]['resposta']){
                $acertos++;
            }
             
        }
    } 
    fecha($pdo, $result); // fecha conexão

    if($gabarito){// se o comando SQL não apresentar erro ele chama a função de cadastro
        $ok = cadastrarQuestionario($acertos, $sequencia);
    }
    return $ok;
}

// função que cadastra o questionário do usuário no banco
// Número de Acertos, ID do usuário e o Dia que foi realizado
function cadastrarQuestionario($acertos, $sequencia){
    $pdo = conn(); //abre conexão
    $ok = false;

    if($pdo != null){

        $query = "INSERT INTO questionarios (acertos, usuarios_idUsuarios, diaQuestionario) VALUES  (:acertos, :idUsuario, NOW())"; // NOW() ele pega o tempo exato que ta dando insert no questionário
        $result = execSQL($pdo, $query);
        $result->bindValue(':acertos', $acertos, PDO::PARAM_STR);
        $result->bindValue(':idUsuario', $_SESSION['id'], PDO::PARAM_STR);
        $salvo = $result->execute();
        $idQuest = $pdo->lastInsertId();

    }
    fecha($pdo, $result);// fecha conexão

    if($salvo){//se for salvo no banco ele chama a função de cadastrar as perguntas
        $ok = cadastrarPerguntas($idQuest, $sequencia);
    }
    return $ok;
}

//função que cadastra a pergunta e o idQuestionario no banco
//Tabela intermediária do banco
function cadastrarPerguntas($idQuest, $sequencia){
    $pdo = conn(); // abre conexão
    $ok = false;

    if($pdo != null){
        
            $query = "INSERT INTO questionarios_respondidos (perguntas_id_perguntas, questionarios_idQuestionarios) VALUES ";

            for($i=0; $i<sizeof($sequencia); $i++){
                $query .= "(".$sequencia[$i]['pergunta'].",$idQuest),";
            }
            $query = substr($query, 0, -1);
            
            $result = execSQL($pdo, $query);
            $salvo = $result->execute();
        
    }
    if($salvo){// se for salvo chama a função de cadastrar as resposat do usuário
        $ok = cadastrarRespostas($idQuest, $sequencia);
        return $ok;
    }else{
        return $ok;
    }
}

//função que cadastra na tabela respostasUsuarios as alternativas que ele respondeu
function cadastrarRespostas($idQuest, $sequencia){
    $pdo = conn();// abre conexao
    $salvo = false;

    if($pdo != null){
        
        $query = "INSERT INTO respostasUsuarios (opcaoUsuario, questionarios_idQuestionarios, perguntas_id_perguntas) VALUES ";

        for($i=0; $i<sizeof($sequencia); $i++){
            $query .= "(\"".$sequencia[$i]['resposta']."\",$idQuest,".$sequencia[$i]['pergunta']."),";
        }
        $query = substr($query, 0, -1);

        $result = execSQL($pdo, $query);
        $salvo = $result->execute();
    }
    fecha($pdo, $result);//fecha conexão

    if($salvo){//se for cadastrado retorna true, se não, false
       return $salvo;
    }else{
        return $salvo;
    }
}
?>