<?php
//puxa as perguntas do banco dados para iniciar um novo questionário
function criarQuestionario(){
    $pdo = conn();// abre conexão

    if($pdo != null){
        $query = "SELECT id_perguntas, pergunta FROM perguntas ORDER BY RAND()";
		$result = execSQL($pdo, $query);
		$result -> execute();
        $pergunta = 1;
        $sequencia = array();
        $i = 0;

        //enquanto tiver perguntas dentro da variável ele irá listar no no questioário
        while( $row = $result->fetch(PDO::FETCH_ASSOC)){
            $sequencia[$i]['pergunta'] = $row['id_perguntas'];
            
            echo "<br/><br/><label for=\"".$row['id_perguntas']."\"><strong>$pergunta) ".utf8_encode($row['pergunta'])."<i class=\"error\">* </i></strong></label><br/><br/>";
            
            $pdoAlt = conn();// abre coneção
            if($pdoAlt != null){
                $queryAlt = "SELECT * FROM listar_alternativas WHERE idPergunta=:id ORDER BY RAND()";
                $resultAlt = execSQL($pdoAlt, $queryAlt);
                $resultAlt -> bindValue(":id", $row['id_perguntas'], PDO::PARAM_STR);
                $resultAlt -> execute();
                
                //enquanto tiver alternativa da pergunta ele irá listar todas
                while( $rowAlt = $resultAlt->fetch(PDO::FETCH_ASSOC)){

                    echo "<input id=\"".$row['id_perguntas'].$rowAlt['alternativa']."\" type=\"radio\" name=\"".$row['id_perguntas']."\" value=\"".$rowAlt['alternativa']."\" maxlength=\"1\">";
                    echo "<label for=\"".$row['id_perguntas'].$rowAlt['alternativa']."\">".utf8_encode($rowAlt['alternativa_enunciado'])."</label><br/>";
                }
            }
            fecha($pdoAlt, $resultAlt);// fecha conexão
            echo "<br/><br/>";
            $pergunta++;
            $i++;
        }
        fecha($pdo, $result); //fecha conexão

        return $sequencia;
    }
}
?>