<?php 
    //função que destrói a sessão quando o usuário clica em sair
    if(isset($_GET['sair'])){
        session_destroy();
        header("Location: ../login.php");
        die();
    }

    //função que pega o nome passado pela sessão
    function pegarNome(){
        $nome = $_SESSION['login'];
        return $nome;
    }

    //função que pega o ID do usuário da sessão
    function pegarID(){
        $id = $_SESSION['id'];
        return $id;
    }
?>