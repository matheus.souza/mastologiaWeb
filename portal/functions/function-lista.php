<?php 
//pelo ID do usuário ele vai listar todos questionários feitos pelo usuário
function listarQuestionarios($id) {
    $pdo = conn(); //abre conexão

    if($pdo != null){
        $query = "SELECT dia_realizado, acertos, IDquest FROM lista_questionarios WHERE ID=:id";
        $result = execSQL($pdo, $query);
        $result -> bindValue(":id", $id, PDO::PARAM_STR);
        $result -> execute();
        $idQuestionario = 1; // contador de questionário

        while( $row = $result->fetch(PDO::FETCH_ASSOC)){
            echo "<tr>";
            echo "<td>$idQuestionario</td>";
            echo "<td>".date('d/m/Y', strtotime($row['dia_realizado']))."</td>";
            echo "<td>".$row['acertos']."/20</td>";
            echo "<td><a href=\"listar-id.php?idQ=".$row['IDquest']."\"><i class=\"fa fa-eye\"></i></a></td>";
            $idQuestionario++;
        }
        fecha($pdo, $result); //fecha conexão
    }

}

//função que lista o questionário selecionado pelo ID
function imprimirQuestionarioID($idQ){
    $pdo = conn(); // abre conexão

    if($pdo != null){
        //select na View que lista o questionário pelo ID
        $query = "SELECT pergunta, idPergunta, gabarito FROM listar_questionarioID WHERE idQuest=:id";
        $result = execSQL($pdo, $query);
        $result -> bindValue(":id", $idQ, PDO::PARAM_STR);
        $result -> execute();
        $idQuestionario = 1;

        //enquanto tiver elemento dentro do $row ele vai dar echo no enunciado da pergunta
        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            echo "<br/><br/><strong>$idQuestionario) ".utf8_encode($row['pergunta'])."</strong><br/><br/>";
            $pdoAlt = conn(); // abre conexão
            if($pdoAlt != null){
                //select na view da alternativa respondida pelo usuário
                $queryAlt = "SELECT respostaUsuario, alternativa_enunciado FROM listar_resposta WHERE id=:id AND idQuest=$idQ";
                $resultAlt = execSQL($pdoAlt, $queryAlt);
                $resultAlt -> bindValue(":id", $row['idPergunta'], PDO::PARAM_STR);
                $resultAlt -> execute();
                $rowAlt = $resultAlt->fetch(PDO::FETCH_ASSOC);

                //comparando se a resposta é igual ao gabarito dar echo em uma classe correta
                if($row['gabarito'] == $rowAlt['respostaUsuario']){
                    echo "<p class=\"correta\">".utf8_encode($rowAlt['alternativa_enunciado'])." <i class=\"fa fa-check\"></i></p>";
                }else{
                    //se não dar echo em uma classe erro
                    echo "<p class=\"error\">".utf8_encode($rowAlt['alternativa_enunciado'])." <i class=\"fa fa-times\"></i></p>";
                }
                fecha($pdoAlt, $resultAlt); //fecha conexão
            }
            
            $idQuestionario++;
        }
        fecha($pdo, $result); //fecha conexão
    }
}
?>