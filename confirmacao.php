<?php
include("functions/function-cadastro.php");
pegarDados(); //função que inicia o procedimento de validação e cadastro
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Woman Care - Cadastro</title>
	<link href="css/style.css" type="text/css" rel="stylesheet" />
	<link href="css/login.css" type="text/css" rel="stylesheet" />
	<link href="css/cadastro.css" type="text/css" rel="stylesheet" />

	<!-- Bootstrap core CSS -->
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="css/full-slider.css" rel="stylesheet">
</head>

<body>

	<div id="login-box">
		<div class="gradiente-claro login-label">
			Confirmação de Cadastro de usuário
		</div>

		<div class="login-rodape" id="cdtro">
			<?php 
			//se der erro ao tentar cadastrar o usuário irá apresentar mensagem de erro
			if(!$erro){
				echo '<h1 class="login-outros left" style="font-size: 20px; text-align: center;">Cadastro Realizado! <br/><a href="login.php">Faça o Login</a></h1><br/>';
			} else{
				echo '<h1 class="login-outros left" style="font-size: 20px; text-align: center;">Erro ao Cadastrar! <br/><a href="javascript:history.go(-1)">Voltar</a></h1><br/>';
			}
			echo $msg; 
			?>
		</div>
	</div>

	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>

</html>s