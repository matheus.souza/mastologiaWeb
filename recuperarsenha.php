<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Woman Care - Recuperar Senha</title>
	<link href="css/style.css" type="text/css" rel="stylesheet" />
	<link href="css/login.css" type="text/css" rel="stylesheet" />
	<link href="css/cadastro.css" type="text/css" rel="stylesheet" />

	<!-- Bootstrap core CSS -->
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="css/full-slider.css" rel="stylesheet">
</head>

<body>
	<!-- Menu da página -->
	<?php include "menu.php" ?>

	<div id="cadastro-box">
		<div class="gradiente-claro login-label">
			Recuperar Senha
		</div>
		<!-- Formulário de recuperação de senha -->
		<form action="functions/function-recuperar.php" name="recuperar" method="post">
			<div class="input-div" id="input-email">
				<input type="email" placeholder="E-mail" name="email"/>
			</div>

			<!-- Campo de preenchimento do RA -->
			<div class="input-div" id="input-RA">
				<input type="text" placeholder="RA" name="RA" maxlength="8"/>
			</div>

			<!-- Campo de preenchimento da Senha -->
			<div class="input-div" id="input-senha">
				<input type="password" placeholder="Senha Nova" name="senha"/>
			</div>

			<!-- Campo de preenchimento da senha de confirmação -->
			<div class="input-div" id="input-confirmacao">
				<input type="password" placeholder="Confirme a Senha" name="confirmar"/>
			</div>

			<!-- Campo de preenchimento do Captcha -->
			<div class="cap login-rodape">
				<img src="functions/captcha/captcha.php" />
				<input type="text" name="palavra" />
				<button type="submit" id="cstro-btn">Recuperar</button>
			</div>
		</form>
		<div class="login-rodape" id="cdtro">
			<span class="login-outros">Já é cadastrado?
				<a href="login.php">Faça o Login</a>
			</span>
			<br/>

		</div>
	</div>
	
	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
  	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>

</html>