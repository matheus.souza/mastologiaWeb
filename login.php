<?php 
	// Todas as páginas que possuem algo relacionado a login do usuário possui a inicialização da sessão
	session_start();
	include("functions/function-login.php");
	
	/* 	Verificando se existe uma sessão de login ativa,
		Caso seja verdadeira irá redirecionar para a página inicial do portal. */
	if(isset($_SESSION["autenticado"])){
		header("Location: portal/index.php"); 
	}
	
	/* Ao clicar no botão de entrar chamará a função de pegar os dados que está dentro do arquivo "function-login" */
	if(isset($_POST["entrar"])){
		pegarDados();
	}
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Woman Care - Login</title>
	<link href="css/style.css" type="text/css" rel="stylesheet" />
	<link href="css/login.css" type="text/css" rel="stylesheet" />

	<!-- Bootstrap core CSS -->
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="css/full-slider.css" rel="stylesheet">
</head>

<body>
	<!-- Menu da página -->
	<?php include "menu.php" ?>

	<div id="login-box">
		<div class="gradiente-claro login-label">
			Login do usuário
		</div>
		<form action="" name="login" method="post">
			<div class="input-div" id="input-ra">
				<input type="text" placeholder="RA" name="RA" maxlength="8" />
			</div>

			<div class="input-div" id="input-senha">
				<input type="password" placeholder="Senha" name="senha" />

			</div>

			<div class="cap login-rodape">
				<span class="login-outros">
					<a href="recuperarsenha.php">Lembrar Senha</a>
				</span>
				<button type="submit" id="login-btn" name='entrar' value="Login">Entrar</button>

			</div>
		</form>
		<div class="login-rodape" id="cdtro">
			<span class="login-outros">Não é cadastrado?
				<a href="cadastro.php">Cadastre-se</a>
			</span>
			<br/>

		</div>
	</div>

	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>