<?php
session_start();//Iniciando a sessão

$codigoCaptcha = substr(md5( time() ) ,0,5); //Gerando o código aleatorio

$_SESSION['captcha'] = $codigoCaptcha;// Salvando o código na sessão para comparação

$imagemCaptcha = imagecreatefrompng("../../img/fundocaptch.png"); //criando captcha a partir do fundo

$fonteCaptcha = imageloadfont("anonymous.gdf"); //criação da fonte utilizada no captcha

$corCaptcha = imagecolorallocate($imagemCaptcha,255,255,255);//cor da fonte

imagestring($imagemCaptcha,$fonteCaptcha,15,5,$codigoCaptcha,$corCaptcha);//colocando a fonte gerada na imagem 

header("Content-type: image/png");//alterando o nome do arquivo para png

imagepng($imagemCaptcha); //envia a imagem gerada no navegador

imagedestroy($imagemCaptcha); //desalocando memoria utilizada da imagem
?>