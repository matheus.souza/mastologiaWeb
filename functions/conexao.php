<?php

function conn(){
    $servidor = "localhost";
    $usuario = "root";
    $senha = "";
    $nomeDB = "mastologia_bd";

    $pdo = null;
    try {
        $pdo = new PDO("mysql:host=$servidor;dbname=$nomeDB", $usuario, $senha);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       
        }
    catch(PDOException $e)
        {
        echo "Connection failed: " . $e->getMessage();
        }
        return $pdo;
}

function execSQL($pdo,$query){
	$result = null;	
	if ($pdo != null){
		try {
			$result = $pdo->prepare($query);
		} catch (PDOException $e){
			echo "<span>Não foi possível realizar a selação!</span>";
		}	
	} else {
		echo "<span><br />Conexão não estabelecida!</span>";
	}
	return $result;
}

function fecha($pdo, $result){	
	$pdo = null;
	$result = null;
}

?>