<?php
include("conexao.php");

/* A função pegarDados irá verificar se todos os campos de entrada da página login foram preenchidos.
Se sim ele irá chamar a função que verifica se exite o RA no banco de dados.
Se todos os campos não estiverem preenchidos ele apresentará uma mensagem de erro que voltara para a página de login.
*/
function pegarDados(){
	$msgErro ="";
	$erro = false;

	global $erro, $msgErro;

	if($_POST["RA"] != null ){
		$RA = ($_POST["RA"]);
		/* Chama a função que verifica se o RA é existente */
		if(!verificarRA($RA)){
			$erro = true;
			$msgErro .= "RA Inexistente!\\n";
		}

	}else{
		$erro = true;
		$msgErro .= "RA não informado!\\n";
	}

	if($_POST["senha"] != null ){
		$senha = ($_POST["senha"]);
	}else{
		$erro = true;
		$msgErro .= "Digite a Senha!\\n";
	}
	
	if(!$erro) {
		/* validarUsuário é uma função que verifica se o usuário digitou a senha e o RA corretamente, se o RA não bater com a senha, a variável usuário irá retornar false que fará apresentar o erro de senha incorreta. */
		$usuario = validarUsuario($RA, $senha);

		/* caso o usuário for válido iniciará a sessão de login do usuário e armazenará em uma variável de sessão o nome o o ID do usuário que será usado dentro do portal. */
		if($usuario != null){
			$_SESSION["autenticado"] = true;
			$_SESSION["login"] = $usuario[0]["nomeCompleto"];
			$_SESSION["id"] = (int) $usuario[0]["idUsuarios"];
			header("Location: portal/index.php"); /* Redireciona para a Tela de Usuario */
		}else{
			$msgErro .= "Login Incorreto!";
			echo "<script>alert('$msgErro'); window.location.href='login.php';</script>";
		}
	}else {
		echo "<script>alert('$msgErro'); window.location.href='login.php';</script>" ;
	}
	
}

/* função que procura no banco de dados o usuário e a senha  */
function validarUsuario($RA, $senha){
	$pdo = conn(); // conexão com o banco de dados

	if($pdo != null){
		$usuario = null; // iniciando uma variável com valor null
		$query = "SELECT nomeCompleto, idUsuarios FROM usuarios WHERE RA=:RA AND senha=:senha"; //comando mysql que irá dar select no usuário
		$result = execSQL($pdo, $query);
		$result->bindValue(':RA', $RA, PDO::PARAM_STR);
		$result->bindValue(':senha', $senha, PDO::PARAM_STR);
		$result -> execute();
		$usuario = $result -> fetchAll(); // armazenando os dados existentes do banco, se não tiver retornado nenum valor ele continuará null

		if($usuario != null){
			return $usuario;
		}else{
			return $usuario;
		}
	}
	fecha($pdo, $result); //fecha conexão pdo do banco
}

// função que verifica se o RA preenchido é existente no banco de dados
function verificarRA($RA){
	$pdo = conn(); //abre a conexão
	$existente = false; //iniciando a variável false

	if($pdo != null){
		$query = "SELECT RA FROM usuarios WHERE RA=:RA"; // irá procurar no banco de dados se o RA existe
		$result = execSQL($pdo, $query);
		$result -> bindValue(":RA", $RA, PDO::PARAM_STR);
		$result -> execute();
		$RAs = $result -> fetch();

		if($RAs > 0){// se o valor retornado tiver algum elemento rentorna true
			$existente = true;
		}else{
			$existente = false;
		}
	}
	fecha($pdo, $result); // fecha conexão
	return $existente;
}


?>