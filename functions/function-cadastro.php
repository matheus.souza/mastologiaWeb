<?php
// Todas as páginas que possuem algo relacionado a login do usuário possui a inicialização da sessão
session_start();
include("conexao.php");

//Quando o formulário da submit na página de cadastro ele chama a função pegar dados
function pegarDados(){
	$msg ="";
	$erro = false;

	global $erro, $msg;

	//Verifica o captcha se foi digitado corretamente
	if ($_POST["palavra"] == null){
		$erro = true;
		$msg .= "Digite o Captcha! <br/>";
	}else if($_POST["palavra"] != $_SESSION["captcha"]){
		/* Se estiver incorreto irá barrar */	
		$erro = true;
		$msg .= "Captcha incorreto! <br/>";
	}

	//verifica se o campo não está vazio
	if(($_POST["nome"]) != null){
		$nome = ($_POST["nome"]);
	} else {
		$erro = true;
		$msg .= "Nome não informado! <br/>";
	}

	//verifica se o campo não está vazio, se ele é valido ou se já é cadastrado
	if($_POST["email"] != null ){
		$email = ($_POST["email"]);
		if (validarEmail($email)){
			$erro = true;
			$msg .= "E-mail Inválido! <br/>";
		}else if(verificarEmail($email)){
			$erro = true;
			$msg .= "E-mail já cadastrado! <br/>";
		} 
	}else{
		$erro = true;
		$msg .= "E-mail não informado! <br/>";
	}

	//verifica se a senha foi digitada
	if($_POST["senha"] != null ){
		$senha = ($_POST["senha"]);
	}else{
		$erro = true;
		$msg .= "Senha não informada! <br/>";
	}

	//verifica se o campo está vazio ou se o RA já foi cadastrado
	if($_POST["RA"] != null ){
		$RA = ($_POST["RA"]);
		if(strlen($RA) < 8){
			$erro = true;
			$msg .= "RA Inválido! <br/>";
		}else if(verificarRA($RA)){
			$erro = true;
			$msg .= "RA já cadastrado! <br/>";
		}

	}else{
		$erro = true;
		$msg .= "RA não informado! <br/>";
	}

	//se nenhum erro for apresentado ele chama a função cadastrarUsuario passando os parametros
	if(!$erro) {
		$cadastrado = cadastrarUsuario($nome, $email, $senha, $RA);

		if(!$cadastrado){
			$erro = true;
		}
	}

}

// função que da Insert dos dados de cadastro do usuário no banco
function cadastrarUsuario($nome, $email, $senha, $RA){
	$salvo = false;
	$pdo = conn(); // abre conexão

	if($pdo != null){
		$query = "INSERT INTO usuarios (nomeCompleto, email, senha, RA)
		VALUES  (:nome, :email, :senha, :RA)";
		$result = execSQL($pdo, $query);
		$result->bindValue(':nome', $nome, PDO::PARAM_STR);
		$result->bindValue(':email', $email, PDO::PARAM_STR);
		$result->bindValue(':senha', $senha, PDO::PARAM_STR);
		$result->bindValue(':RA', $RA, PDO::PARAM_STR);

		$salvo = $result->execute();
	}
	fecha($pdo, $result); // fecha conexão
	return $salvo;
}

// função de verificação de email existente no banco de dados
function verificarEmail($email){
	$pdo = conn();
	$repetido = false;
	if($pdo != null){
		$query = "SELECT email FROM usuarios WHERE email=:email";
		$result = execSQL($pdo, $query);
		$result -> bindValue(":email", $email, PDO::PARAM_STR);
		$result -> execute();
		$emails = $result -> fetch();

		if($emails > 0){
			$repetido = true;
		}else{
			$repetido = false;
		}
	}
	fecha($pdo, $result);
	return $repetido;
}

// função que verifica se o RA é existente no banco de dados
function verificarRA($RA){
	$pdo = conn();
	$repetido = false;
	if($pdo != null){
		$query = "SELECT RA FROM usuarios WHERE RA=:RA";
		$result = execSQL($pdo, $query);
		$result -> bindValue(":RA", $RA, PDO::PARAM_STR);
		$result -> execute();
		$RAs = $result -> fetch();

		if($RAs > 0){
			$repetido = true;
		}else{
			$repetido = false;
		}
	}
	fecha($pdo, $result);
	return $repetido;
}

// função de validar o email digitado
function validarEmail($email){
	if(filter_var($email, FILTER_VALIDATE_EMAIL)){
		return false;
	}else{
		return true;
	}
}

?>