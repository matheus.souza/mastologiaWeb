<?php
session_start();
include("conexao.php");
pegarDados(); // pega os dados do post

function pegarDados(){
	$msg ="";
	$erro = false;

	global $erro, $msg;

	if ($_POST["palavra"] == null){
		$erro = true;
		$msg .= "Digite o Captcha!\\n";
	}else if($_POST["palavra"] != $_SESSION["captcha"]){
		/* Se estiver incorreto irá barrar */	
		$erro = true;
		$msg .= "Captcha Incorreto!\\n";
	}

	if($_POST["email"] != null ){
		$email = ($_POST["email"]);
		if (validarEmail($email)){
			$erro = true;
			$msg .= "E-mail Inválido!\\n";
		}
	}else{
		$erro = true;
		$msg .= "E-mail não informado!\\n";
	}

	if($_POST["RA"] != null ){
		$RA = ($_POST["RA"]);

		if(!verificarRA($RA)){
			$erro = true;
			$msg .= "RA Inexistente!\\n";
		}

	}else{
		$erro = true;
		$msg .= "RA não informado!\\n";
	}

	if($_POST["confirmar"] == $_POST["senha"]){
		if($_POST["confirmar"] != null ){
			$senha = ($_POST["confirmar"]);
		}else{
			$erro = true;
			$msg .= "Confirme a Senha!\\n";
		}

		if($_POST["senha"] != null ){
			$senha = ($_POST["senha"]);
		}else{
			$erro = true;
			$msg .= "Digite a Senha!\\n";
		}
	}else{
		$erro = true;
		$msg .= "Senha de confirmação não confere com senha!\\n";
	}
	
	// se não tiver erro chama a função de atualização da senha do usuário
	if(!$erro) {
		$atualizado = atualizarUsuario($email, $RA, $senha);

		if($atualizado){
			$msg .= "<script>alert('Senha alterada!'); window.location.href=\"../login.php\";</script>";
			echo $msg;
		}else{
			$msg .= "<script>alert('Cadastro não encontrado!'); window.history.go(-1);</script>";
			echo $msg;
		}
	}else {
		echo "<script>alert('$msg'); window.history.go(-1);</script>";
	}

}

// Função que da update na senha do usuário de acordo com o email e ra passado
function atualizarUsuario($email, $RA, $senha){
	$pdo = conn();
	$loginCorreto = false;

	if($pdo != null){
		$query = "UPDATE usuarios SET senha=:senha WHERE RA=:RA AND email=:email";
		$result = execSQL($pdo, $query);
		$result->bindValue(':email', $email, PDO::PARAM_STR);
		$result->bindValue(':RA', $RA, PDO::PARAM_STR);
		$result->bindValue(':senha', $senha, PDO::PARAM_STR);
		$atualizado = $result -> execute();
	}
	fecha($pdo, $result);

	// se não der erro no update o valor retornará true
	return $atualizado;
}

//Verificar se o RA está cadastrado no banco
function verificarRA($RA){
	$pdo = conn();
	$existente = false;
	if($pdo != null){
		$query = "SELECT RA FROM usuarios WHERE RA=:RA";
		$result = execSQL($pdo, $query);
		$result -> bindValue(":RA", $RA, PDO::PARAM_STR);
		$result -> execute();
		$RAs = $result -> fetch();

		if($RAs > 0){
			$existente = true;
		}else{
			$existente = false;
		}
	}
	fecha($pdo, $result);
	return $existente;
}

//valida o campo de email
function validarEmail($email){
	if(filter_var($email, FILTER_VALIDATE_EMAIL)){
		return false;
	}else{
		return true;
	}
}
?>