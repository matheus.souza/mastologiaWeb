<!DOCTYPE html>
<html lang="pt-br">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Woman Care</title>

	<!-- Bootstrap core CSS -->
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="css/full-slider.css" rel="stylesheet">

	<!-- CSS customizado -->
	<link href="css/style.css" rel="stylesheet">

</head>

<body>

	<?php include "menu.php" ?>

	<!-- Coursel de Imagens da Página -->
	<header>
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner" role="listbox">
				<!--Primeiro Slide - Outubro Rosa -->
				<div class="carousel-item active" style="background-image: url('./img/img3.jpg')">
					<div class="carousel-caption d-none d-md-block descricao">
						<h3 class="font-preta">Outubro Rosa</h3>
						<p>Outubro Rosa é uma campanha de conscientização para todas mulheres para prevenção do cancer de mama.</p>
					</div>
				</div>
				<!-- Mamógrafo -->
				<div class="carousel-item" style="background-image: url('./img/img2.jpg')">
					<div class="carousel-caption d-none d-md-block descricao">
						<h3>Mamógrafo</h3>
						
					</div>
				</div>
				<!-- Radiologista -->
				<div class="carousel-item" style="background-image: url('./img/img1.jpg')">
					<div class="carousel-caption d-none d-md-block descricao">
						<h3>Radiologista</h3>
						
					</div>
				</div>
			</div>
			<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</header>

	<!-- Conteúdo -->
	<section class="py-5">
		<div class="container">
			<!-- O que é Bioinformática? -->
			<h1>Bioinformática</h1>
			<img src="img/bioinformatica.jpg" class="redonda esq"/>
			<p>A bioinformática é uma ciência multidisciplinar que surgiu da necessidade de se compreender as funções biológicas, mais especificamente os genes. A engenharia de softwares, a matemática, a física, a química, a estatística, a ciência da computação e a biologia molecular são algumas áreas do conhecimento relacionadas a ela.</p>
			<p>A bioinformática abriu portas para que novas tecnologias surgissem para ajudar na compreensão e no estudo do corpo humano. Entre as áreas da mecidina a bioinformática é aplicada na área de <strong>Mastologia</strong>.</p>
			<div class="clear"></div>
			<br/><br/>

			<!-- O que é Mastologia? -->
			<h1>Mastologia</h1>
			<img src="img/mamografo.jpg" class="redonda dir"/>
			<p>Especialidade médica que é responsável pelo cuidado com a saúde das mamas. É esse especialista quem examina, diagnostica e trata possíveis problemas dessa região do corpo feminino.</p>
			<p>As primeiras máquinas que surgiram com o conceido de bionfirmática voltadas para mamografia surgiram em 1966. Elas emitem feixe de Raios-X com maior penetração para interagir no tecido mamário.</p>
			<p>A imagem gerada pelo mamógrafo possui uma alta qualidade para verificar minúsculos detalhes no tecido mamário e fazer a sua análise.</p>
			<div class="clear"></div>
			<br/><br/>

			<!-- Materiais/Artigos -->
			<h1>Materiais</h1>
			<p>Para saber mais sobre a <strong>Bioinformática</strong> e a <strong>Mastologia</strong> leia esses artigos:</p>
			<ul class="pdf">
				<li><a href="arquivos/Artigo1_Bioinformatica.pdf" target="_blank">A era da Bioinformática: seu Potencial e suas Implicações para as Ciências da Saúde</a></li>
				<li><a href="arquivos/Artigo2_Mastologia_B.pdf" target="_blank">Banco de Imagens Mamográficas para Treinamento na Interpretação de Imagens Digitais</a></li>
				<li><a href="arquivos/Artigo3_Mastologia_A.PDF" target="_blank"><em>Mammographic Assessment of a Geographically Defined Population at a Mastology Referral Hospital in São Paulo Brazil</em></a></li>
			</ul>
		</div>
	</section>

	<?php include "rodape.php" ?>

	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>