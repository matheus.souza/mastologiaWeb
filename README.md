# Mastologia Web

Projeto feito na aula de Bioinformática para criar um sistema de questionário com os artigos disponibilizados por tema. 

O tema escolhido foi Mastologia.

## Template

A página inicial foi usado template do [Start Bootstrap](https://startbootstrap.com) - [Half Slider](https://startbootstrap.com/template-overviews/half-slider/).

## Linguagens

* PHP
* HTML
* CSS
* JavaScript
* SQL

## Banco de dados

* MySQL

## Frameworks

* [Bootstrap](https://getbootstrap.com)
* [jQuery](https://jquery.com)

## Autor

* **Matheus de Souza** - Projeto de Faculdade - [GitLab](https://gitlab.com/matheus.souza/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details