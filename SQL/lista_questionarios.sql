CREATE VIEW lista_questionarios as select 
usuarios.idUsuarios "ID", 
questionarios.diaQuestionario "dia_realizado", 
questionarios.acertos "acertos", 
questionarios.idQuestionarios "IDquest" 
from usuarios inner join questionarios 
on usuarios.idUsuarios = questionarios.usuarios_idUsuarios;