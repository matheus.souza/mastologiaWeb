CREATE DATABASE mastologia_bd;
USE mastologia_bd ;

-- -----------------------------------------------------
-- Table Usuarios
-- -----------------------------------------------------
CREATE TABLE mastologia_bd.usuarios (
  idUsuarios INT NOT NULL AUTO_INCREMENT,
  nomeCompleto VARCHAR(160) NOT NULL,
  email VARCHAR(60) NOT NULL,
  senha VARCHAR(16) NOT NULL,
  RA VARCHAR(8) NOT NULL,
  PRIMARY KEY (idUsuarios));


-- -----------------------------------------------------
-- Table Perguntas
-- -----------------------------------------------------
CREATE TABLE mastologia_bd.perguntas (
  id_perguntas INT NOT NULL AUTO_INCREMENT,
  pergunta VARCHAR(800) NOT NULL,
  resposta CHAR(1) NOT NULL,
  PRIMARY KEY (id_perguntas));


-- -----------------------------------------------------
-- Table Alternativas
-- -----------------------------------------------------
CREATE TABLE  mastologia_bd.alternativas (
  idAlternativas INT NOT NULL AUTO_INCREMENT,
  enunciado VARCHAR(120) NOT NULL,
  alternativas CHAR(1) NOT NULL,
  perguntas_id_perguntas INT NOT NULL,
  PRIMARY KEY (idAlternativas),
  FOREIGN KEY (perguntas_id_perguntas) REFERENCES mastologia_bd.perguntas (id_perguntas));


-- -----------------------------------------------------
-- Table Questionarios
-- -----------------------------------------------------
CREATE TABLE mastologia_bd.questionarios (
  idQuestionarios INT NOT NULL AUTO_INCREMENT,
  acertos INT NOT NULL,
  diaQuestionario TIMESTAMP NOT NULL,
  usuarios_idUsuarios INT NOT NULL,
  PRIMARY KEY (idQuestionarios),
  FOREIGN KEY (usuarios_idUsuarios) REFERENCES mastologia_bd.usuarios (idUsuarios));


-- -----------------------------------------------------
-- Table Resposta Usuarios
-- -----------------------------------------------------
CREATE TABLE mastologia_bd.respostasUsuarios (
  idRespostas INT NOT NULL AUTO_INCREMENT,
  opcaoUsuario CHAR(1) NOT NULL,
  questionarios_idQuestionarios INT NOT NULL,
  perguntas_id_perguntas INT NOT NULL,
  PRIMARY KEY (idRespostas),
  FOREIGN KEY (questionarios_idQuestionarios) REFERENCES mastologia_bd.questionarios (idQuestionarios),
  FOREIGN KEY (perguntas_id_perguntas) REFERENCES mastologia_bd.perguntas (id_perguntas));


-- -----------------------------------------------------
-- Table Questionarios respondidos
-- -----------------------------------------------------
CREATE TABLE  mastologia_bd.questionarios_respondidos (
  perguntas_id_perguntas INT NOT NULL,
  questionarios_idQuestionarios INT NOT NULL,
  FOREIGN KEY (perguntas_id_perguntas) REFERENCES mastologia_bd.perguntas (id_perguntas), 
  FOREIGN KEY (questionarios_idQuestionarios) REFERENCES mastologia_bd.questionarios (idQuestionarios));

-- -----------------------------------------------------
-- Populando Perguntas
-- -----------------------------------------------------
INSERT INTO perguntas (id_perguntas, pergunta, resposta) VALUES 
(1,'Mamografia  é o método mais eficiente para detectar tumores e anomalias na mama. Quais das doenças abaixo se destaca como  um tumor de mama:','A'),
(2, 'O diagnóstico para detectar tumores são realizado em pacientes que tem sintomas específico, tais como:','D'),
(3,'Para que a  qualidade da imagem da mamografia seja respeitado para um rastreamento bem sucedido qual a margem de erro:','C'),
(4,'Segundo as estatísticas um  a cada  ____ mulheres pode desenvolver câncer de mama. Sendo ___ descobre quando está em um nível avançado.','C'),
(5,'O Colégio Americano de Radiologia criou um sistema especial a fim de:','A'),
(6,'O sistema desenvolvido pelo colégio de radiologia  além de eficaz precisa de  hardwares com algumas especificações para que funcione corretamente, dentre eles:','D'),
(7,'Dentre as inúmeras funcionalidades que o sistema desenvolvido pelo colégio de radiologia permite, uma delas é:','C'),
(8,'Os benefícios do sistema desenvolvido pelo colégio de radiologia são: <br/><br/><ol type="I"><li>Alto custo, porém tem o diagnóstico imediato.</li><li>Software que faz parte de um departamento de treinamento.</li><li>Baixo custo além de ser gratuito.</li><li>Software para uso de qualquer pessoa, desde que tenha um conhecimento prévio do assunto.</li><li>Uso de profissionais da área interessado na interpretação de imagem.</ol>','D'),
(9,'Câncer de mama é uma das principais mortes do mundo, quais desses países abaixo a taxa de mortalidade por causa da doença é maior?','D'),
(10,'Quando implementaram o rastreio de mamografia qual foi a porcentagem de redução de mortalidade ?','A'),
(11,'Em 2004 um programa de Saúde de Mama da Região Norte do Hospital  Geral de Vila Nova Cachoeirinha, começou a disponibilizar diagnóstico e tratamento para mulheres que contia câncer de mama. Qual a finalidade desse programa?','B'),
(12,'Qual o total de mulheres que usufruíram do programa de saúde de mama de Vila Nova Cachoeirinha ?','D'),
(13,'Em ____, o Colégio Americano de Radiografia e ao  _________________________ desenvolveu um sistema para  ______  mamográficos.','A'),
(14,'Em qual local  o Colégio Americano de Radiografia as estatísticas mamográficas estavam sendo registrada na década de 1990?','A'),
(15,'Quais fatores levam um observador de radiologia mamografia tirar diferentes conclusões da mesma mamografia?','B'),
(16,'Uma mamografia feita corretamente auxilia a detecção de câncer de mama por qual motivo?','C'),
(17,'"Bioinformática surgiu em 1980 a fim de superar as fronteiras da ciência pelo desenvolvimento de noca  abordagens capazes de promover a análise e apresentação de dados biológico"<br/><br/>De acordo com o texto acima assinale verdadeiro ou falso para definição de bioinformatica:<ol type="I"><li>Bioinformática é a aplicação da tecnologia na área biológica.</li><li>Bioinformática desenvolve  ferramentas computacionais para ajudar pessoas de baixa renda.</li><li>Bioinformática desenvolve  ferramentas computacionais para ajudar a descobrir novos métodos de cura.</li><li>Cria programas analitivso capazes de armazenar complexas sequências de genes.</li><li>Estuda o porquê de tecnologia na área da saúde.</li></ol>','A'),
(18,'Quantas proteínas contém em um organismo humano ativo?','C'),
(19,'A Bioinformática tem sido um grande avanço na área da  saúde, mas ainda existem vários desafios a serem enfrentados, um deles seria?','A'),
(20,'"Nao seria _____ afirma que a  ________________ é uma ________ para o sucesso de análise de dados dos projetos".','B');

-- -----------------------------------------------------
-- Populando Alternativas
-- -----------------------------------------------------
INSERT INTO alternativas (enunciado, alternativas, perguntas_id_perguntas) VALUES 
('Microcalcificação','A',1),
('Astrocitomas','B',1),
('Linfoma','C',1),
('Ependimomas','D',1),
('Adenocarcinomas','E',1),
('Cansaço, mudança de humor e alteração no paladar e olfato','A',2),
('Perda excessiva de peso, febres e calafrio','B',2),
('Fome Excessiva, cansaço e Fluxo papilar','C',2),
('Fluxo papilar e alteração no seios','D',2),
('Cansaço, alteração no seios e fome','E',2),
('0% a 30%','A',3),
('10% a 12%','B',3),
('5% a 15%','C',3),
('20% a 37%','D',3),
('15% a 22%','E',3),
('10, 50%','A',4),
('12, 80%','B',4),
('12, 75%','C',4),
('10, 75%','D',4),
('12, 80%','E',4),
('Expressar achados mamográficos em categorias, padronizando laudo médico','A',5),
('Sistema barato que atinge toda a população','B',5),
('Uniformizar  a linguagem médica','C',5),
('Achar vulnerabilidade em mamografia','D',5),
('Ter resultados rápidos','E',5),
('Notebook com 8GB de memória e tela de 17 polegadas','A',6),
('Monitores de 17 polegadas com resolução mínima de 1.280x 1.024 pixels','B',6),
('Monitor de 15 polegadas com resolução mínima de 1.072 x 1.000 pixels e 8 bits por pixel','C',6),
('Monitor de 17 polegadas com resolução mínima de 1.208 x 1.024 e 8 bits por pixel','D',6),
('Monitores de 17 polegadas com resolução mínima de 1.280x 1.050 pixels','E',6),
('Ver todas as mamografias','A',7),
('Realizar  diagnóstico profundo sobre a doença','B',7),
('Permite treinamento de profissionais da área','C',7),
('Consegue monitorar quantas mamografia a pessoa realizou ao longo da vida','D',7),
('Soluções de tratamento','E',7),
('Apenas V está correto','A',8),
('Apenas I e II estão corretos','B',8),
('Apenas I, II e IV estão corretos','C',8),
('Apenas II e III e V estão corretos','D',8),
('Todos estão corretos','E',8),
('Indonésia e Brasil','A',9),
('Índia e Argentina','B',9),
('Estados Unidos e Brasil','C',9),
('Estados Unidos e na América do Norte','D',9),
('Europa e América do Norte ','E',9),
('25% - 35%','A',10),
('10% - 35%','B',10),
('0% - 35%','C',10),
('35%','D',10),
('25%','E',10),
('Levar assistência a unidades básicas de saúde para todas pessoas','A',11),
('Levar assistência a unidades básicas de saúde para pessoas de baixa renda','B',11),
('Verificar a quantidade de mulheres que possui o câncer','C',11),
('Delimitar tratamento ao câncer','D',11),
('Meio do programa ganhar dinheiro em cima de unidades básicas carente','E',11),
('1.000','A',12),
('10.000','B',12),
('7.540','C',12),
('7.508','D',12),
('5.224','E',12),
('1992, a sociedade Brasileira de mastologia, relatório','A',13),
('1882, o Estados unidos, relatório','B',13),
('1992, a sociedade Canadense, registrar','C',13),
('1980, a sociedade Brasileira de mastologia, relatório','D',13),
('2000, a sociedade Canadense, relatório','E',13),
('Excel','A',14),
('Banco de dados Access','B',14),
('Word','C',14),
('Nuvem','D',14),
('Banco de dados biológico','E',14),
('Má resolução da imagem','A',15),
('Cansaço físico de horas de estudo da imagem','B',15),
('Falta de Habilidade','C',15),
('Desconcentrações','D',15),
('Falta de vontade','E',15),
('Melhor visualização da glândula mamária','A',16),
('Resultado imediato','B',16),
('Permite que o tecido mamário seja radiografado','C',16),
('Permite a visualização da mama completa','D',16),
('Nenhuma das alternativas','E',16),
('V,F,V,V,F','A',17),
('V,V,V,F,V','B',17),
('F,V,V,F,F','C',17),
('V.F.V,F,F','D',17),
('F,F,V,F,V','E',17),
('Contém 8 proteínas que assume a conformação específica.','A',18),
('Contém 208 mil proteínas que assume a conformação específica.','B',18),
('Contém 50 mil proteínas que assume a conformação específica.','C',18),
('Contém 1050 mil proteínas  que assume a conformação específica.','D',18),
('Contém 500 proteínas  que assume a conformação específica.','E',18),
('Escassez de mão de obra para desenvolver o software','A',19),
('Software inapropriados para o tratamento','B',19),
('Funcionários dispostos a trabalhar com tecnologia','C',19),
('Nenhuma das alternativas','D',19),
('Todas as alternativas contribui para esse desafio','E',19),
('Essencial, Biologia, peça-chave','A',20),
('Exagero, bioinformática, peça-chave','B',20),
('Mentira, bioinformática, essencial','C',20),
('Exagero, Biologia, peça-chave','D',20),
('Absurdo, tecnologia, avanço','E',20);

-- -----------------------------------------------------
-- Criando View de Listar Perguntas
-- -----------------------------------------------------
CREATE VIEW lista_questionarios as select usuarios.idUsuarios "ID", questionarios.diaQuestionario "dia_realizado", questionarios.acertos "acertos", questionarios.idQuestionarios "IDquest" from usuarios inner join questionarios on usuarios.idUsuarios = questionarios.usuarios_idUsuarios;

-- -----------------------------------------------------
-- Criando View de Listar Alternativas
-- -----------------------------------------------------
CREATE VIEW listar_alternativas as select perguntas.id_perguntas "idPergunta", alternativas.alternativas "alternativa", alternativas.enunciado "alternativa_enunciado" from perguntas 
inner join alternativas on perguntas.id_perguntas = alternativas.perguntas_id_perguntas;

-- -----------------------------------------------------
-- Criando View de Listar Perguntas dos Questionarios Cadastrados
-- -----------------------------------------------------
CREATE VIEW listar_questionarioID as select 
tab_r.questionarios_idQuestionarios "idQuest", 
tab_r.perguntas_id_perguntas "idPergunta", 
tab_p.pergunta "pergunta", 
tab_p.resposta "gabarito" from questionarios_respondidos tab_r
inner join perguntas tab_p on tab_p.id_perguntas = tab_r.perguntas_id_perguntas;

-- -----------------------------------------------------
-- Criando View de Listar Alternativas Respondidas por usuarios de Questionarios Cadastrados
-- -----------------------------------------------------
CREATE VIEW listar_resposta as select 
tab_r.perguntas_id_perguntas "id",
tab_r.questionarios_idQuestionarios "idQuest",
tab_r.opcaoUsuario "respostaUsuario",
tab_a.enunciado "alternativa_enunciado" 
from respostasUsuarios tab_r 
inner join alternativas tab_a on tab_r.perguntas_id_perguntas = tab_a.perguntas_id_perguntas and tab_r.opcaoUsuario = tab_a.alternativas;