CREATE VIEW listar_resposta as select 
tab_r.perguntas_id_perguntas "id",
tab_r.questionarios_idQuestionarios "idQuest",
tab_r.opcaoUsuario "respostaUsuario",
tab_a.enunciado "alternativa_enunciado" 
from respostasUsuarios tab_r 
inner join alternativas tab_a on tab_r.perguntas_id_perguntas = tab_a.perguntas_id_perguntas and tab_r.opcaoUsuario = tab_a.alternativas;