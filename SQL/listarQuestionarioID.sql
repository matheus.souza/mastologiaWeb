CREATE VIEW listar_questionarioID as select 
tab_r.questionarios_idQuestionarios "idQuest", 
tab_r.perguntas_id_perguntas "idPergunta", 
tab_p.pergunta "pergunta", 
tab_p.resposta "gabarito" from questionarios_respondidos tab_r
inner join perguntas tab_p on tab_p.id_perguntas = tab_r.perguntas_id_perguntas;